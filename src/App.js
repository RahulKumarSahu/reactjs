import { useState, useEffect } from "react";
import axios from "axios";
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

const App = () => {

  const [candidates, setCandidates] = useState([]);

  // console.log(candidates);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [dob, setDob] = useState('');
  const [gender, setGender] = useState('');
  const [state, setState] = useState('');
  const [pimage, setPhoto] = useState(null);
  const [rdoc, setRdoc] = useState(null);
  const [location, setlocation] = useState([]);

  const [updateName, setUpdateName] = useState('');
  const [updateEmail, setUpdateEmail] = useState('');
  const [updateDob, setUpdateDob] = useState('');
  const [updateGender, setUpdateGender] = useState('');
  const [updateLocation, setUpdatelocation] = useState([]);
  const [updateState, setUpdateState] = useState('');
  const [updatePimage, setUpdatePhoto] = useState(null);
  const [updateRdoc, setUpdateRdoc] = useState(null);
  const [updateUserId, setUpdateUserId] = useState("");
  const [show, setShow] = useState(false);

  useEffect(() => {
    candidateData()
  }, [])

  const handleChange = (e) => {
    if (e.target.name == 'name') {
      setName(e.target.value)
    }
    if (e.target.name == 'email') {
      setEmail(e.target.value)
    }
    if (e.target.name == 'dob') {
      setDob(e.target.value)
    }
    if (e.target.name == 'gender') {
      setGender(e.target.value)
    }
    if (e.target.name == 'state') {
      setState(e.target.value)
    }
    if (e.target.name == 'rdoc') {
      setRdoc(e.target.files[0])
    }
    if (e.target.name == 'pimage') {
      setPhoto(e.target.files[0]);
    }
    if (e.target.name == 'location') {
      const { value, checked } = e.target;
      if (checked) {
        setlocation([...location, value]);
      } else {
        setlocation(location.filter((location) => location !== value));
      }
    }
  };


  const handleUpdate = (e) => {
    if (e.target.name == 'name') {
      setUpdateName(e.target.value)
    }
    if (e.target.name == 'emails') {
      setUpdateEmail(e.target.value)
    }
    if (e.target.name == 'dobs') {
      setUpdateDob(e.target.value)
    }
    if (e.target.name == 'genders') {
      setUpdateGender(e.target.value)
    }
    if (e.target.name == 'states') {
      setUpdateState(e.target.value)
    }
    if (e.target.name == 'rdocs') {
      setUpdateRdoc(e.target.files[0])
    }
    if (e.target.name == 'pimages') {
      setUpdatePhoto(e.target.files[0]);
    }
    if (e.target.name == 'locations') {
      const { value, checked } = e.target;
      if (checked) {
        setUpdatelocation([...location, value]);
      } else {
        setUpdatelocation(location.filter((location) => location !== value));
      }
    }
  };

  const handleClose = () => {
    setShow(false)
    setName("")
    setEmail("")
    setDob("")
    setGender("")
    setPhoto("")
    setlocation("")
    setRdoc("")

    setUpdateName("")
    setUpdateEmail("")
    setUpdateDob("")
    setUpdateGender("")
    setUpdatelocation("")
    setUpdateState("")
    setUpdatePhoto("")
    setUpdateRdoc("")
  };

  const handleShow = (id) => {
    // console.log(id, "userid")
    setShow(true);
    // setUserId(id);
    EditCandidate(id);
  }

  const handleDelete = (id) => {
    deleteCandidate(id);
  }

  const candidateData = () => {
    var config = {
      method: 'post',
      url: 'http://127.0.0.1:8000/api/candidate',
      headers: {}
    };

    axios(config)
      .then(function (response) {
        setCandidates(response.data.message);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  const EditCandidate = (id) => {

    // var data = new FormData();
    // data.append('id', setUserId);

    let data = {
      id: id
    }

    var config = {
      method: 'post',
      url: 'http://127.0.0.1:8000/api/edit',
      headers: {
      },
      data: data
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        setUpdateUserId(response.data.data.id)
        setUpdateName(response.data.data.name);
        setUpdateEmail(response.data.data.email);
        setUpdateDob(response.data.data.dob);
        setUpdateGender(response.data.data.gender);
        setUpdatelocation(response.data.data.location);
        setUpdateState(response.data.data.state);
        setUpdatePhoto(response.data.data.pimage);
        setUpdateRdoc(response.data.data.rdoc);
      })
      .catch(function (error) {
        console.log(error);
      });


  }

  const deleteCandidate = (id) => {
    var data = new FormData();
    data.append('id', id);

    var config = {
      method: 'post',
      url: 'http://127.0.0.1:8000/api/destroy',
      headers: {
      },
      data: data
    };

    axios(config)
      .then(function (response) {
        console.log(response.data);
        candidateData();

      })
      .catch(function (error) {
        console.log(error);
      });

  }

  const handleSubmit = (e) => {

    e.preventDefault();

    var data = new FormData();
    data.append('name', name);
    data.append('email', email);
    data.append('dob', dob);
    data.append('gender', gender);
    data.append('state', state);
    data.append('location', location);
    data.append('pimage', pimage);
    data.append('rdoc', rdoc);

    var config = {
      method: 'post',
      url: 'http://127.0.0.1:8000/api/resume',
      headers: {
        'Accept': 'application/json',

      },
      data: data
    };

    axios(config)
      .then(function (response) {
        // console.log(JSON.stringify(response.data));
        candidateData();
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  const onUpdate = (e) => {
    console.log(e);
    e.preventDefault();

    var data = new FormData();
    data.append('id', updateUserId);
    data.append('name', updateName);
    data.append('email', updateEmail);
    data.append('dob', updateDob);
    data.append('gender', updateGender);
    data.append('state', updateState);
    data.append('location', updateLocation);
    data.append('pimage', updatePimage);
    data.append('rdoc', updateRdoc);

    var config = {
      method: 'post',
      url: 'http://127.0.0.1:8000/api/update',
      headers: {
        'Accept': 'application/json',
      },
      data: data
    };

    axios(config)
      .then(function (response) {
        handleClose();
        candidateData();
      })
      .catch(function (error) {
        console.log(error);
      });


  }

  return (
    <div className="App">
      <div className="Container-fluid">
        <div className="col-md-12">
          <div className="row m-2">
            <h2 className="text-center border border-primary mt-3 p-2 rounded bg-danger">Resume</h2>
            <div className="col-md-5">
              <form className="m-3" onSubmit={handleSubmit}>
                <div class="mb-1">
                  <label for="name" className="form-label">Name</label>
                  <input type="text" className="form-control" name="name" value={name} onChange={handleChange} />
                </div>

                <div class="mb-1">
                  <label for="name" className="form-label">Email</label>
                  <input type="email" className="form-control" name="email" value={email} onChange={handleChange} />
                </div>

                <div className="mb-1">
                  <label for="dob" className="form-label">dob</label>
                  <input type="date" className="form-control" name="dob" value={dob} onChange={handleChange} />
                </div>
                <div className="mb-1 row">
                  <label for="" className="form-label">Gender</label>
                  <div className="form-check col-md-2">
                    <input className="form-check-input" type="radio" name="male" checked={gender === 'male'} value="male" onChange={handleChange} />
                    <label className="form-check-label">Male</label>
                  </div>
                  <div className="form-check col-md-2">
                    <input className="form-check-input" type="radio" name="female" checked={gender === 'female'} value="female" />
                    <label className="form-check-label">Female</label>
                  </div>
                </div>

                <div className="mb-1">
                  <label for="exampleInputPassword1" className="form-label">State</label>
                  <select name="state" value={state} onChange={handleChange}>
                    <option value="">--Select--</option>
                    <option value="delhi">Delhi</option>
                    <option value="jaipur">Jaipur</option>
                    <option value="mumbai">Mumbai</option>
                  </select>
                </div>

                <div className="mb-1 row" >
                  <label for="exampleInputPassword1" className="form-label">Location</label>
                  <div class="form-check col-md-2">
                    <input class="form-check-input" type="checkbox" checked={location.includes('Delhi')} value="Delhi" />
                    <label class="form-check-label">Delhi</label>
                  </div>

                  <div class="form-check col-md-2">
                    <input class="form-check-input" type="checkbox" checked={location.includes('Mumbai')} value="Mumbai" />
                    <label class="form-check-label">Mumbai</label>
                  </div>

                  <div class="form-check col-md-2">
                    <input class="form-check-input" type="checkbox" checked={location.includes('Jaipur')} value="Jaipur" />
                    <label class="form-check-label">Jaipur</label>
                  </div>

                  <div class="form-check col-md-2">
                    <input class="form-check-input" type="checkbox" checked={location.includes('Bangalore')} value="Banglore" />
                    <label class="form-check-label"> Banglore</label>
                  </div>
                </div>

                <div class="mb-1">
                  <label for="name" className="form-label">Pimage</label>
                  <input type="file" className="form-control" name="pimage" accept="image/*" multiple onChange={handleChange} />
                  <img src={pimage} />
                </div>

                <div class="mb-1">
                  <label for="name" className="form-label">Rdoc</label>
                  <input type="file" className="form-control" name="rdoc" accept="image/*" multiple onChange={handleChange} />
                  <img src={rdoc} />
                </div>

                <button type="submit" className="btn btn-primary">Submit</button>
              </form>
            </div>
            <div className="col-md-7">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Image</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {candidates.map((item, index) => {
                    {/* console.log(item?.id,"loopppp") */ }
                    return <tr>
                      <th scope="row">{index + 1}</th>
                      <td>{item.name}</td>
                      <td>{item.email}</td>
                      <td><img className="img-fluid" src={'http://127.0.0.1:8000/pimages/' + item.pimage} />
                      </td>
                      <td>
                        <button type="button" class="btn btn-primary mt-1" onClick={() => handleShow(item?.id)}>Edit</button>
                        <button type="button" class="btn btn-danger mt-1" onClick={() => handleDelete(item?.id)}>Delete</button>
                      </td>
                    </tr>
                  })}

                </tbody>
              </table>
            </div>
          </div>

          <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
              <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div class="mb-1">
                <label for="name" className="form-label">Name</label>
                <input type="text" className="form-control" name="name" value={updateName} onChange={handleUpdate} />
              </div>

              <div class="mb-1">
                <label for="name" className="form-label">Email</label>
                <input type="email" className="form-control" name="email" value={updateEmail} onChange={handleUpdate} />
              </div>

              <div className="mb-1">
                <label for="dob" className="form-label">dob</label>
                <input type="date" className="form-control" name="dobs" value={updateDob} onChange={handleUpdate} />
              </div>
              <div className="mb-1 row">
                <label for="" className="form-label">Gender</label>
                <div className="form-check col-md-2">
                  <input className="form-check-input" type="radio" name="male" checked={updateGender === 'male'} value="male" onChange={handleUpdate} />
                  <label className="form-check-label">Male</label>
                </div>
                <div className="form-check col-md-2">
                  <input className="form-check-input" type="radio" name="female" checked={updateGender === 'female'} value="female" />
                  <label className="form-check-label">Female</label>
                </div>
              </div>

              <div className="mb-1">
                <label for="exampleInputPassword1" className="form-label">State</label>
                <select name="states" value={updateState} onChange={handleUpdate}>
                  <option value="">--Select--</option>
                  <option value="delhi">Delhi</option>
                  <option value="jaipur">Jaipur</option>
                  <option value="mumbai">Mumbai</option>
                </select>
              </div>

              <div className="mb-1 row" >
                <label for="exampleInputPassword1" className="form-label">Location</label>
                <div class="form-check col-md-2">
                  <input class="form-check-input" type="checkbox" checked={updateLocation.includes('Delhi')} value="Delhi" />
                  <label class="form-check-label">Delhi</label>
                </div>

                <div class="form-check col-md-2">
                  <input class="form-check-input" type="checkbox" checked={updateLocation.includes('Mumbai')} value="Mumbai" />
                  <label class="form-check-label">Mumbai</label>
                </div>

                <div class="form-check col-md-2">
                  <input class="form-check-input" type="checkbox" checked={updateLocation.includes('Jaipur')} value="Jaipur" />
                  <label class="form-check-label">Jaipur</label>
                </div>

                <div class="form-check col-md-2">
                  <input class="form-check-input" type="checkbox" checked={updateLocation.includes('Bangalore')} value="Banglore" />
                  <label class="form-check-label"> Banglore</label>
                </div>
              </div>

              <div class="mb-1">
                <label for="name" className="form-label">Pimage</label>
                <input type="file" className="form-control" name="pimages" accept="image/*" multiple onChange={handleUpdate} />
                <img src={updatePimage} />
              </div>

              <div class="mb-1">
                <label for="name" className="form-label">Rdoc</label>
                <input type="file" className="form-control" name="rdocs" accept="image/*" multiple onChange={handleUpdate} />
                <img src={updateRdoc} />
              </div>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              <Button variant="primary" onClick={onUpdate}>
                Save Changes
              </Button>
            </Modal.Footer>
          </Modal>

        </div>
      </div>
    </div>
  );
}

export default App;
